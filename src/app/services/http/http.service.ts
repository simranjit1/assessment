import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { apilist } from './api.list';


@Injectable({
  providedIn: 'root'
})

export class HttpService {
  baseUrl: string = environment.baseUrl;

  constructor(
    private http: HttpClient,
    private apiList: apilist
  ) { }



  httpPost(url: string, params): Observable<Object> {
    return this.http.post(this.baseUrl + this.apiList[url], params);
  }

  httpGetWithQueryParam(url: string, params): Observable<Object> {
    return this.http.get(this.baseUrl + this.apiList[url] + "?page=" + params);
  }


}
