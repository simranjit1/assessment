import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common/common.service';
import { HttpService } from 'src/app/services/http/http.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  signupForm: FormGroup;
  submitted=false;

  constructor(private fb:FormBuilder, private cm: CommonService, private router: Router, private http: HttpService) { }

  ngOnInit() {
    this.signupForm = this.fb.group({
      email:['',[Validators.compose([Validators.required, Validators.pattern(/^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])]],
      password:['',[Validators.required,Validators.minLength(8)]],
      confirmPassword:['',[Validators.required]],
    },
    {
      validator: this.cm.MustMatch('password','confirmPassword')
    })
  }

  get f(){
    return this.signupForm.controls;
  }

  signUp(){
    this.submitted=true;
    if(this.signupForm.valid){
      let param={
        'email':this.signupForm.value.email,
        'password':this.signupForm.value.password
      }
      this.http.httpPost('signup',param).subscribe((res :any) => {
        if(res.success==true){
          this.cm.presentsToast('success','top-end','Signed Up Successfully');
          this.router.navigateByUrl('/auth/login');
        }
        else{
          this.cm.presentsToast('error','top-end',res.message);
        }
      })
    }
  }

}
