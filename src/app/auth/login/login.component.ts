import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/services/common/common.service';
import { HttpService } from 'src/app/services/http/http.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  submitted=false;
  isExist:any;
  constructor(private fb:FormBuilder, private cm: CommonService, private router: Router, private http: HttpService) { }

  ngOnInit() {
    this.isExist=JSON.parse(localStorage.getItem('remember'))
    this.loginForm = this.fb.group({
      email:['',[Validators.compose([Validators.required, Validators.pattern(/^(\d{10}|\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3}))$/)])]],
      password:['',[Validators.required]],
      rememberMe:[false]
    })
    if(this.isExist != undefined && this.isExist != null && this.isExist != ''){
      this.loginForm.controls.email.setValue(this.isExist.email);
      this.loginForm.controls.password.setValue(this.isExist.password);
      this.loginForm.controls.rememberMe.setValue(true);
 }
  }

  get f(){
    return this.loginForm.controls;
  }

  login(){
    this.submitted=true;
    if(this.loginForm.valid){
      let param={
        'email':this.loginForm.value.email,
        'password':this.loginForm.value.password
      }
      this.http.httpPost('login',param).subscribe((res :any) => {
        if(res.success==true){
          this.cm.presentsToast('success','top-end','Logged in Successfully');
          localStorage.setItem('accesstoken',res.data.token);
          this.router.navigateByUrl('/pages/building');
        }
        else{
          this.cm.presentsToast('error','top-end',res.message);
        }
      })
    }
    if(this.loginForm.value.rememberMe==true){
      let remember={
        'email':this.loginForm.value.email,
        'password':this.loginForm.value.password
      }
      localStorage.setItem('remember',JSON.stringify(remember));
    }
    else{
      localStorage.removeItem('remember');
    }
    this.loginForm.value.rememberMe=true;
    
  }

}
