import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http/http.service';

@Component({
  selector: 'app-building-pagination',
  templateUrl: './building-pagination.component.html',
  styleUrls: ['./building-pagination.component.scss']
})
export class BuildingPaginationComponent implements OnInit {

  page = 1;
  allData: any[] = [];
  constructor(private http: HttpService) { }

  ngOnInit() {
    this.http.httpGetWithQueryParam('properties', this.page).subscribe((res: any) => {
      this.allData = res;
      console.log(this.allData);
    })
  }

  listOnScroll() {
    console.log(this.page);
    this.page++;
    
    this.http.httpGetWithQueryParam('properties', this.page).subscribe((res: any) => {
       this.allData.concat(res);
    })
  }

}
