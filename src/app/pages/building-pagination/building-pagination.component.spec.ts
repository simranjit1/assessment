import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildingPaginationComponent } from './building-pagination.component';

describe('BuildingPaginationComponent', () => {
  let component: BuildingPaginationComponent;
  let fixture: ComponentFixture<BuildingPaginationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuildingPaginationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildingPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
