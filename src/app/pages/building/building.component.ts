import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import * as data from '../../../assets/properties.json';

@Component({
  selector: 'app-building',
  templateUrl: './building.component.html',
  styleUrls: ['./building.component.scss']
})
export class BuildingComponent implements OnInit {

  buildings:any[]=[];
  arr:any[]=[];
  price=new FormControl();
  constructor() { }

  ngOnInit() {
    this.buildings=data['default'].data
    this.arr = data['default'].data
  }

  change(){
    this.buildings=[];
    this.arr.forEach(element => {
      if(element.min_price>this.price.value){
        this.buildings.push(element)
      }
    });
  }

}
