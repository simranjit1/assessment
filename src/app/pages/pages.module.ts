import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BuildingComponent } from './building/building.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { BuildingPaginationComponent } from './building-pagination/building-pagination.component';


@NgModule({
  declarations: [
    PagesComponent,
    BuildingComponent,
    BuildingPaginationComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    ReactiveFormsModule,
    InfiniteScrollModule
  ]
})
export class PagesModule { }
