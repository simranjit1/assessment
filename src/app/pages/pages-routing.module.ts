import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../services/auth/auth.service';
import { BuildingPaginationComponent } from './building-pagination/building-pagination.component';
import { BuildingComponent } from './building/building.component';
import { PagesComponent } from './pages.component';

const routes: Routes = [
  { path: '', component: BuildingComponent},
  { path: 'building', component: BuildingComponent},
  { path: 'building-pagination', component: BuildingPaginationComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
